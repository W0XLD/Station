# Station

This repository houses scripts and documents pertaining to the Pikes Peak Radio Amateurs Association club station, especially checklists, equipment manuals, and cheat sheets.

Housing these documents in a source code repository allows club members to discuss them in the issue tracker, and to make use of collaborative tools like Wikis to develop them further.
