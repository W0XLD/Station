# Maintenance Checklist

Year: ____________ Circle one: **Winter Spring Summer Fall**

## Mow/trim antenna farm
*Not needed for winter maintenance*

Name: ________________________________

Date completed: ________________

Notes:

## Inspect antennas
Check for tower and mast integrity.

Check for broken/fraying guys.

Check for damaged feedlines.

Name: ________________________________

Date completed: ________________

Notes:

## Run backup generator
Use shop vac as load; run for 10-15
minutes.

Name: ________________________________

Date completed: ________________

Notes:

## Fuel up generator and top off gas can
Add Sta-Bil fuel stabilizer according to ratio on bottle.

Name: ________________________________

Date completed: ________________

Notes:

## Inventory station equipment
*Check station logbook for any equipment removed or returned*

Note if any station is missing: radio, tuner, CPU, monitor, keyboard, mouse. Station 1 is the north
station, 2 is the middle, 3 is to the south.

Name: ________________________________

Date completed: ________________

Notes:

## Vacuum station floor
*Shop vac is in the storage building*

Name: ________________________________

Date completed: ________________

Notes:

## [Battery backup?]
[Describe checks and maintenance for batteries in storage building]

Name: ________________________________

Date completed: ________________

Notes:

--
Please report damage, missing equipment, and additional required maintenance by emailing
boardmembers@ppraa.org.