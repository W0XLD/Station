# Operations Checklist
## Arrival
* [ ] Record date/time, call sign, and name of arrival in the log book inside door
* [ ] Unlock north entrance to accommodate wheelchair-bound users
* [ ] Turn on two main breakers in southeast corner of trailer
* [ ] Trash bag in trash can
    * *Trash bags are to the right, inside the door of the storage building*
* [ ] Storage building key is returned to shelf above station 2

## Operation
* Record date/time, call sign, and name of all station users in log book inside door
* Record summary of use, any equipment removed or returned in log book inside door
* Station 2 radio powered up by putting 30 A (green) fuse in fuse carrier in power supply cable
* If contacts are made using AF0S club call sign, log and report later to boardmembers@ppraa.org
    * *If PPRAA receives any QSL cards at the club PO box, we can confirm the contacts from your logs*

## Departure
* [ ] Storage building is closed and locked
* [ ] Storage building key is returned to shelf above station 2
* [ ] Station 2 30 A (green) fuse removed from fuse carrier, placed next to monitor at station 2
* [ ] All three stations cleaned/straightened up; paper and personal belongings removed
* [ ] All heaters are turned off and unplugged from the outlets
* [ ] Rotator turned off
* [ ] Computers shut down gracefully
* [ ] Computers and radios covered with trash bags
* [ ] All coaxial cables disconnected from antenna patch panel
* [ ] Trash bag removed from trash can, and taken with last user to depart station
* [ ] Record date/time of station closure in log book inside door
* [ ] North entrance door latched and bolted securely
* [ ] Turn off two main breakers in southeast corner of trailer
* Ensure all “Arrival" and “Departure" checklist items are finished and initialed, above
* Print name, sign, and date/time this checklist below, and leave in binder
* Latch main door and padlock securely

Name/call sign (printed):______________________________ Signature:______________________________

Date/time:______________________